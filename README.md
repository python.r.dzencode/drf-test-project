# Proxy To Bitrix:
### Login logic, specify the fields:
#### Use link: ' /api/auth/login/ '
    {
        "email": "email for user account",
        "password": "password for user account"
    }

****
### Registration logic, specify the fields:
#### User link: ' /api/auth/register '
    {
        "email": "email for user account",
        "password": "password for user account"
    }

****
### Forgot Password logic, specify the fields:
### Use link: ' /api/auth/forgot-pass/ '
    {
        "email": "email for user account"
    }

****
### Reset Password logic, specify the fields:
### Use link: ' /api/auth/reset-pass/ '
    {
        "old_password": "old user password",
        "new_password": "new user password"
    }

****
# Favorites:
### To create a favorite, specify the fields:
#### Use link: ' /api/favorites/create/ '
    {
        "email": "user email",
        "obj_id" "id for this favorite",
        "item": {"key", vakue}  # json item
    }

****
### To update a favorite, specify the fields:
#### Use link: ' /api/favorites/update/ '
    {
        "fav_pk": "primary key for favorite",
        "email": "user email",
        "obj_id": "id for this favorite",
        "item": {"key", vakue}  # json item
    }

****
### To read a favorite by primary key for favorite:
#### Use link: ' /api/favorites/view_by_id/<int:id_>/'
    Where <int:id_> this id for favorite.

****
### To get all favorites for user
#### Use link: ' /api/favorites/<int:id_>/by_user/ '
    Where <int:id_> this id for user.

****
### To delete all favorites by user id
#### Use link: ' /api/favorites/delete_by_user/ '
    {
        "user_pk": "primary key for user"
    }