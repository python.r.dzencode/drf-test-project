from django.contrib.auth.models import User

from rest_framework import serializers

from auth_app.models import BitrixUser


class UserDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        exclude = ('id', 'password', )


class BitrixUserDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = BitrixUser
        fields = '__all__'


class RegisterLoginBitrixUserSerializer(serializers.ModelSerializer):

    password = serializers.CharField(max_length=155)

    class Meta:
        model = BitrixUser
        fields = ('email', 'password')

    def create(self, validated_data):
        email = validated_data.get('email', None)

        user = BitrixUser.objects.create(email=email)
        user.save()

        return user


class ForgotPassBitrixUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = BitrixUser
        fields = ('email', )


class ResetPassBitrixUserSerializer(serializers.Serializer):

    old_password = serializers.CharField(max_length=155)
    new_password = serializers.CharField(max_length=155)
