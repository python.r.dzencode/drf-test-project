from django.db import models


class BitrixUser(models.Model):
    id = models.BigAutoField(primary_key=True, unique=True, verbose_name='ID')
    email = models.EmailField(null=False, blank=False, db_index=True, verbose_name='Email')

    updated = models.DateTimeField(auto_now=True, verbose_name='Последнее обновление')
    timestamp = models.DateTimeField(auto_now_add=True, verbose_name='Создан')

    def __str__(self):
        return f'{self.pk} {self.email}'
