import logging
import json
import re
from time import time

import requests

from django.conf import settings

from rest_framework.response import Response

from auth_app.models import BitrixUser
from auth_app.serializers import RegisterLoginBitrixUserSerializer

BX_LOGIN_URL = f'{settings.BX_HOST}/api/login.php'
BX_REGISTRATION_URL = f'{settings.BX_HOST}/api/register.php'
BX_FORGOT_PASSWORD_URL = f'{settings.BX_HOST}/api/forgot_password.php'
BX_RESET_PASSWORD_URL = f'{settings.BX_HOST}/api/reset_password.php'

logging.basicConfig(filename=settings.LOGFILE, level=logging.INFO)


class BitrixAuth:
    BX_AUTH_KEY = settings.BX_AUTH_KEY

    @classmethod
    def login_post_get_response(cls, user_login: dict, curr_time: float) -> dict:
        data = user_login
        data['AUTH_KEY'] = cls.BX_AUTH_KEY
        logging.info(f"BEFORE REQUEST TO BX: {time() - curr_time}")
        response = requests.post(url=BX_LOGIN_URL, data=data)
        logging.info(f"RECEIVE DATA FROM BX: {time() - curr_time}")

        return json.loads(response.content)

    @staticmethod
    def login_post_check_response(response_data: dict) -> Response:
        if 'USER_ID' in response_data['message']:
            # save user if not exist
            return Response(
                {"user_id": response_data['message']['USER_ID'], "response": response_data})
        else:
            try:
                return Response({"error": response_data['message']['0']})
            except Exception as ex:
                return Response({"error": response_data})

    @classmethod
    def registration_post_get_response(cls, user_data: dict) -> tuple:
        data = user_data
        data['AUTH_KEY'] = cls.BX_AUTH_KEY
        response = requests.post(url=BX_REGISTRATION_URL, data=data)
        logging.info(f"Bitrix registration response: {response.content}")

        return json.loads(response.content), data

    @staticmethod
    def registration_post_check_response(response_data: dict, data: dict) -> Response:
        if response_data['status']:
            re_id = re.compile('\d+')
            try:
                user_id = re_id.findall(response_data['message'])[0]
                data['id'] = user_id
                user_reg = RegisterLoginBitrixUserSerializer(data=data)
                if user_reg.is_valid(raise_exception=True):
                    user_reg.id = user_id
                    us = user_reg.save()
                    old_pk = us.pk
                    us.id = user_id
                    us.save()
                    BitrixUser.objects.get(id=old_pk).delete()
                return Response({"user_id": user_id, "response": response_data})
            except Exception as ex:
                logging.error(ex)
                return Response({"error": response_data})  # , status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({"error": response_data})  # , status=status.HTTP_400_BAD_REQUEST)

    @classmethod
    def forgot_password_post_get_response(cls, user_login: str) -> dict:
        data = {'user_login': user_login, 'AUTH_KEY': cls.BX_AUTH_KEY}
        response = requests.post(url=BX_FORGOT_PASSWORD_URL, data=data)

        return json.loads(response.content)

    @classmethod
    def reset_password_post_get_response(cls, user_data: dict) -> dict:
        data = user_data
        data['AUTH_KEY'] = cls.BX_AUTH_KEY
        response = requests.post(url=BX_RESET_PASSWORD_URL, data=data)

        return json.loads(response.content)
