from django.contrib import admin

from auth_app.models import BitrixUser


@admin.register(BitrixUser)
class BitrixUserAdmin(admin.ModelAdmin):
    list_display = ('id', 'email', 'updated', 'timestamp')
    list_display_links = ('email', )
    fields = (
        'email',
        'updated',
        'timestamp'
    )
    readonly_fields = ('email', 'updated', 'timestamp')
