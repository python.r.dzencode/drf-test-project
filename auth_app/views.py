import time

from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from auth_app import serializers, services

app_templates_dir = 'auth_app/'


class LoginViewSet(ViewSet):
    """
        Proxy view to Bitrix login logic.
    """

    serializer_class = serializers.RegisterLoginBitrixUserSerializer

    def login(self, request, *args, **kwargs):
        data = request.data
        serializer = self.serializer_class(data, many=False)

        user_data = {'user_login': serializer.data.get('email'), 'user_pass': serializer.data.get('password')}

        response = services.BitrixAuth.login_post_get_response(user_login=user_data, curr_time=time.time())

        return services.BitrixAuth.login_post_check_response(response_data=response)


class RegisterViewSet(ViewSet):
    """
        Proxy view to Bitrix Registration logic.
    """

    serializer_class = serializers.RegisterLoginBitrixUserSerializer

    def create(self, request, *args, **kwargs) -> Response:
        data = request.data
        serializer = self.serializer_class(data, many=False)

        user_data = {'user_email': serializer.data.get('email'), 'user_pass': serializer.data.get('password')}

        response_data, data = services.BitrixAuth.registration_post_get_response(user_data=user_data)

        data['email'] = data['user_email']
        data['password'] = data['user_pass']

        return services.BitrixAuth.registration_post_check_response(response_data=response_data, data=data)


class ForgotPasswordViewSet(ViewSet):
    """
        Proxy view to Bitrix forgot password logic.
    """

    serializer_class = serializers.ForgotPassBitrixUserSerializer

    def forgot_pass(self, request, *args, **kwargs):
        data = request.data
        serializer = self.serializer_class(data, many=False)
        email = serializer.data.get('email')

        response = services.BitrixAuth.forgot_password_post_get_response(user_login=email)
        return Response(response)


class ResetPasswordViewSet(ViewSet):
    """
        Proxy view to Bitrix reset password logic.
    """

    serializer_class = serializers.ResetPassBitrixUserSerializer

    def reset_pass(self, request, *args, **kwargs):
        data = request.data
        serializer = self.serializer_class(data, many=False)

        user_data = serializer.data

        response = services.BitrixAuth.reset_password_post_get_response(user_data=user_data)
        return Response(response)
