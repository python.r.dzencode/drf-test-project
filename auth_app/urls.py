from django.urls import path

from auth_app import views

app_name = 'auth_app_app'

urlpatterns = [
    path('login/', views.LoginViewSet.as_view({'post': 'login'}), name='login_view'),
    path('register/', views.RegisterViewSet.as_view({'post': 'create'}), name='register_view'),
    path('forgot-pass/', views.ForgotPasswordViewSet.as_view({'post': 'forgot_pass'}), name='forgot_password'),
    path('reset-pass/', views.ResetPasswordViewSet.as_view({'post': 'reset_pass'}), name='reset_password'),
]
