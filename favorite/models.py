from django.db import models
from django.db.models import QuerySet

from auth_app.models import BitrixUser


class Favorite(models.Model):

    id = models.BigAutoField(primary_key=True, verbose_name='ID')

    owner = models.ForeignKey(BitrixUser, on_delete=models.CASCADE, db_index=True, null=False, related_name='favorite')
    obj_id = models.CharField(max_length=250, verbose_name='Идентификатор объекта')
    item = models.JSONField(null=False, verbose_name='JSON', default=dict)

    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)

    @classmethod
    def get_all_by_user_id(cls, user_pk: int = None) -> QuerySet:
        return cls.objects.filter(owner__pk=user_pk).select_related('owner')

    @classmethod
    def delete_all_by_user_id(cls, user_pk: int = None):
        cls.get_all_by_user_id(user_pk=user_pk).delete()

    def __str__(self):
        return f'{self.pk} {self.obj_id}'
