from django.contrib import admin

from favorite.models import Favorite


@admin.register(Favorite)
class FavoriteAdmin(admin.ModelAdmin):
    list_display = ('id', 'owner', 'obj_id', 'updated_at', 'created_at')
    list_display_links = ('id', 'owner', 'obj_id')
    fields = (
        'owner',
        'obj_id',
        'item',
        'updated_at',
        'created_at',
    )
    ordering = ('-created_at', )
    readonly_fields = ('created_at', 'updated_at')
