from abc import abstractmethod

from rest_framework import serializers, status
from rest_framework.response import Response

from auth_app.serializers import BitrixUserDetailSerializer
from auth_app.models import BitrixUser
from favorite.models import Favorite


class FavoriteSerializer(serializers.ModelSerializer):

    owner = BitrixUserDetailSerializer()

    class Meta:
        model = Favorite
        fields = '__all__'


class BaseFavoriteCreateUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = None
        fields = None
        extra_kwargs = None

    def to_representation(self, instance):
        data = super(BaseFavoriteCreateUpdateSerializer, self).to_representation(instance)

        user_pk = int(data['owner'])
        user = BitrixUser.objects.get(pk=user_pk)

        s = BitrixUserDetailSerializer(instance=user)

        data['owner'] = s.data

        return data


class FavoriteCreateSerializer(BaseFavoriteCreateUpdateSerializer):

    class Meta:
        model = Favorite
        fields = ('owner', 'obj_id', 'item')
        extra_kwargs = {
            'owner': {'required': True},
            'obj_id': {'required': True},
            'item': {'required': True},
        }


class FavoriteUpdateSerializer(BaseFavoriteCreateUpdateSerializer):

    class Meta:
        model = Favorite
        fields = ('owner', 'obj_id', 'item')
        extra_kwargs = {
            'owner': {'required': False},
            'obj_id': {'required': False},
            'item': {'required': True},
        }


class FavoritesDeleteByUserIdSerializer(serializers.Serializer):

    user_pk = serializers.IntegerField(required=True)

    @abstractmethod
    def destroy_all_favorites_by_user_id(self, pk: int = None) -> Response:
        """
            Deleted all favorites by user id and returns Response(status=status.HTTP_204_NO_CONTENT).

            :param pk: user pk.
            :type pk: int.
            :returns: Response(status=status.HTTP_204_NO_CONTENT).
            :type: Response

        """

        Favorite.delete_all_by_user_id(user_pk=pk)

        return Response(status=status.HTTP_204_NO_CONTENT)
