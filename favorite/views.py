from django.views.decorators.cache import cache_page
from django.utils.decorators import method_decorator
from django.conf import settings

from rest_framework.viewsets import ModelViewSet, ViewSet
from rest_framework.response import Response

from favorite import serializers
from favorite.models import Favorite

cache_timeout = settings.DEFAULT_CACHE_TIMEOUT


@method_decorator(cache_page(cache_timeout), name='list')
@method_decorator(cache_page(cache_timeout), name='retrieve')
class FavoriteCreateReadUpdateDeleteViewSet(ModelViewSet):
    """
        Create Read Update Delete Favorite ViewSet.
    """

    model = Favorite

    def get_queryset(self):
        favorites = self.model.objects.select_related('owner')
        return favorites

    def get_serializer_class(self):

        if self.action == 'list' or self.action == 'retrieve':
            return serializers.FavoriteSerializer

        if self.action == 'create':
            return serializers.FavoriteCreateSerializer

        if self.action == 'update':
            return serializers.FavoriteUpdateSerializer

    def create(self, request, *args, **kwargs):
        try:
            favorite = super(FavoriteCreateReadUpdateDeleteViewSet, self).create(request, *args, **kwargs)
            return favorite
        except Exception as ex:
            return Response({'errors': ex.__str__()})

    def update(self, request, *args, **kwargs):
        try:
            response = super(FavoriteCreateReadUpdateDeleteViewSet, self).update(request, *args, **kwargs)
            return response
        except Exception as ex:
            return Response({'errors': ex.__str__()})


@method_decorator(cache_page(cache_timeout), name='list')
class FavoriteReadByUserIdViewSet(ViewSet):
    """
        Read all favorites by user id.
    """
    model = Favorite

    def list(self, request, pk: int = None, *args, **kwargs):

        favorites = self.model.objects.filter(owner__pk=pk).select_related('owner')
        serializer = serializers.FavoriteSerializer(instance=favorites, many=True)
        return Response(serializer.data)


class FavoriteDeleteAllByUserIdViewSet(ViewSet):
    """
        Deleted all favorites by user id.
    """

    model = Favorite

    def get_queryset(self, request, *args, **kwargs):
        return self.model.objects.select_related('owner')

    @staticmethod
    def destroy(request, *args, **kwargs):
        data = request.data
        serializer = serializers.FavoritesDeleteByUserIdSerializer(instance=data, many=False)

        user_pk = serializer.data.get('user_pk')

        response = serializer.destroy_all_favorites_by_user_id(pk=user_pk)

        return response
