from django.urls import path

from favorite import views

app_name = 'favorite'

urlpatterns = [
    path('', views.FavoriteCreateReadUpdateDeleteViewSet.as_view({'get': 'list'}), name='all_favorites_view'),
    path('create/', views.FavoriteCreateReadUpdateDeleteViewSet.as_view({'post': 'create'}), name='create_view'),
    path('read/<int:pk>/', views.FavoriteCreateReadUpdateDeleteViewSet.as_view({'get': 'retrieve'}), name='create_view'),
    path('update/<int:pk>/', views.FavoriteCreateReadUpdateDeleteViewSet.as_view({'put': 'update'}), name='update_view'),
    path('delete/<int:pk>/', views.FavoriteCreateReadUpdateDeleteViewSet.as_view({'delete': 'destroy'}), name='delete_view'),

    path('<int:pk>/by_user/', views.FavoriteReadByUserIdViewSet.as_view({'get': 'list'}), name='read_by_user_id'),
    path('delete_all_by_user_id/', views.FavoriteDeleteAllByUserIdViewSet.as_view({'delete': 'destroy'}), name='delete_all_by_user_id'),
]
